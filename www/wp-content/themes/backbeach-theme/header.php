<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$bootstrap_version = get_theme_mod( 'understrap_bootstrap_version', 'bootstrap4' );
$navbar_type       = get_theme_mod( 'understrap_navbar_type', 'collapse' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<?php wp_enqueue_style( 'understrap-child-styles', get_stylesheet_directory_uri() . '/newstyle.css', wp_get_theme()->get('Version') );?>
	<!-- google fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+3:ital,wght@0,200..900;1,200..900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&family=Source+Sans+3:ital,wght@0,200..900;1,200..900&display=swap" rel="stylesheet">
	
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->


	
	<header id="wrapper-navbar">
		<div class="top-header-holder"> 
			<div class="container">
				<div class="row align-items-end" >
					<div class="col-4">
						<div class="logo-holder"></div>
						<a href="/" class="logo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="the back beach logo" style="width:175px">
						</a>
					</div>
					
					<div class="col-6">
						<div class="reservation-holder">
							<div class="contact-holder">
								<a class="phone" style="text-decoration:none; color:black"><i class="fa fa-phone"></i> +491781072354</a>
								<a class="address" style="text-decoration:none; color:black"><i class="fa fa-map"></i> Mohrenstraße 17, 10117 Berlin</a>
								<!-- top logo for tablet view -->
								<div class="social-media-holder-tablet">
									<a href="#"><i class="fa fa-facebook" style="color:orange"></i></a>
									<a href="#"><i class="fa fa-instagram" style="color:orange"></i></a>
								</div>
							</div>
							<a href="#" class="reservation-btn" style="text-decoration:none; color:black">Make a reservation</a>
						</div>
					</div>

					<div class="col-1">
						<div class="social-media-holder">
							<a href="#"><i class="fa fa-facebook" style="color:orange"></i></a>
							<a href="#"><i class="fa fa-instagram" style="color:orange"></i></a>
						</div>
					</div>
		
				</div>
			</div>
		</div>
					


		<a class="skip-link <?php echo understrap_get_screen_reader_class( true ); ?>" href="#content">
			<?php esc_html_e( 'Skip to content', 'understrap' ); ?>
		</a>

		<?php get_template_part( 'global-templates/navbar', $navbar_type . '-' . $bootstrap_version ); ?>

	</header><!-- #wrapper-navbar -->
