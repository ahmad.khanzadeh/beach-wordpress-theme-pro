<?php
/**
 * Navbar branding
 *
 * @package Understrap
 * @since 1.2.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! has_custom_logo() ) { ?>

	<?php if ( is_front_page() && is_home() ) : ?>

		<h1 class="navbar-brand mb-0">
			<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url">
				<?php bloginfo( 'name' ); ?>
			</a>
		</h1>

	<?php else : ?>
		<!-- 
			**********************************
			here is the place I hide a text from the new theme
			if the name of blog should be displayed anywhere
			just uncomment this place

			commented part in original header
			**********************************
		 -->
		<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url">
			<!-- <?php bloginfo( 'name' ); ?> -->
		</a>
		<!-- 
			=========================================
			end of the place that I comment to not to show to the user
			=========================================
		 -->

	<?php endif; ?>

	<?php
} else {
	the_custom_logo();
}
