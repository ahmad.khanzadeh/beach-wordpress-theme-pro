<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'backbeach' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'DY7C#cYPif$=-tCPz?wY2c&iidh=>+V.Ml7^}:VV{D=cL[__+LqnFC.dh/|?RoG[' );
define( 'SECURE_AUTH_KEY',  '3ccw/m1&%vvU;y)6n*w1jNf]3,}%IH>%NqnDz$CO$q!KxZFLr} ,sn $nApEM<yg' );
define( 'LOGGED_IN_KEY',    'k|*PS$ }t2r{IoS&#*Rz:WtO,d.:yccHmQb9VRDNNhcmdqV_.<mZr$o!c/nqf%-*' );
define( 'NONCE_KEY',        'P]Oh1~/>e_nQelSgpmV} fff!E|9pw%#?1$9AFw.iOA{WAh{9^TIN@yl&fY!PM<V' );
define( 'AUTH_SALT',        '3K5mhW{-B[jtMSTvS>)R*h3G;*]^VPWUI_l{<9&a><h-z>~(B:ME4DTQ$ .akf3h' );
define( 'SECURE_AUTH_SALT', 'i[XrNweA}:R[=27 K%(#7^[*up]C^?U$Ilt;MsePXQk-rp{-5#S71K/fJE`V4qS.' );
define( 'LOGGED_IN_SALT',   'V)Z*EW`3:.[fkU-[~=t%}5Y6x[>NX~qe[9SgjLumd&K_uFcNkRLOkQ2/Y?EW}vQq' );
define( 'NONCE_SALT',       ')a-HH.eno_(*I l_L_e8Ep%vFmDZHEt-X,+RR{.p3M4^V}V2WaZT>>4QLvGCx.sX' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
